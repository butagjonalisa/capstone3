const productsData = [
    {
        id: "H3514G",
        name: "T5-Watches",
        description: "T5 brings to you the perfect everyday wristwatch; combining style and durability guaranteed to give you the best choice.",
        price: 2500.00,
        onOffer: true
    },
    {
        id: "H3587G",
        name: "T5-Watches",
        description: "T5 brings to you the perfect everyday wristwatch; combining style and durability guaranteed to give you the best choice.",
        price: 3020.00,
        onOffer: true
    },
    {
        id: "SNKM94",
        name: "Seiko-Watches",
        description: "Seiko offers high quality watches; combining style, durability, and performance dedicated to perfection and innovation.",
        price: 11599.00,
        onOffer: true
    },
    {
        id: "SYMA22",
        name: "Seiko-Watches",
        description: "Seiko offers high quality watches; combining style, durability, and performance dedicated to perfection and innovation.",
        price: 8549.75,
        onOffer: true
    },
    {
        id: "WB018B",
        name: "Ben Sherman-Watches",
        description: "A leader of modern British style. Ben Sherman has always been for the individuals, for those that set themselves apart. In 1963, we created an iconic look all of our own: sharp, tailored and authentic timepiece.",
        price: 2716.00,
        onOffer: true
    },
    {
        id: "WB025B",
        name: "Ben Sherman-Watches",
        description: "A leader of modern British style. Ben Sherman has always been for the individuals, for those that set themselves apart. In 1963, we created an iconic look all of our own: sharp, tailored and authentic timepiece.",
        price: 3066.00,
        onOffer: true
    }
]

export default productsData;