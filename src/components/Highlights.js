import { Row, Col, Card } from 'react-bootstrap'


export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Apple Watch Series 9</h2>
						</Card.Title>
							<Card.Text>
							Our most powerful chip in Apple Watch ever. A magical new way to use your Apple Watch without touching the screen. A display that’s twice as bright. And now you can choose a watch case and band combination that’s carbon‑neutral.
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Apple Watch Ultra 2</h2>
						</Card.Title>
							<Card.Text>
							The most rugged and capable Apple Watch pushes the limits again. Featuring the all-new S9 SiP. A magical new way to use your watch without touching the screen. The brightest Apple display ever. And now you can choose a case and band combination that is carbon‑neutral.
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Apple Watch SE!</h2>
						</Card.Title>
							<Card.Text>
							Easy ways to stay connected. Motivating fitness metrics. Innovative health and safety features. And now with carbon‑neutral case and band combinations. Apple Watch SE offers totally lovable features at good value.
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}