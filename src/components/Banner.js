import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function Banner() {
  
  return (
    <Row>
      <Col>
        <h1>Igo Watch</h1>
        <p>Everything you need is here!</p>
        <Button variant="primary">Buy now!</Button>
      </Col>
    </Row>
  );
}