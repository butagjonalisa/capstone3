//import productsData from '../data/productData';
import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard'

export default function Products() {

	const [products, setProducts] = useState([]);

	//Retrieve products from database
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return(
					<ProductCard key={product.id} productProp={product} />
				)
			}))
		})
	})

/*	const products = productsData.map(product => {
		return (
			<ProductCard key={product.id} productProp={product} />
		)
	})*/

	return (
		<>
			{products}
		</>
	)
}