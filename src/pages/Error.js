import Banner from "../components/Banner";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Error() {
  const linkStyle = {
    color: "white",
    textDecoration: "none", 
  };
  return (
    <Container>
      <Banner
        title="404 - Not Found"
        subtitle="The page you are looking for cannot be found."
        button={
          <Link to="/" style={linkStyle}>
            Back Home
          </Link>
        }
      />
    </Container>
  );
}
