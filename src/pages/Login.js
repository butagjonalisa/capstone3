import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

    // Prevents page redirection via form submission
    e.preventDefault();
    fetch('http://localhost:4000/users/login',{

    method: 'POST',
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({

        email: email,
        password: password

    })
})
.then(res => res.json())
.then(data => {

    console.log(data.access)

    if(typeof data.access !== "undefined"){

        localStorage.setItem('token',data.access);

        retrieveUserDetails(data.access);

        setUser({
            access: localStorage.getItem('token')
        })

        //alert('You are now logged in');  
        Swal.fire({
            title: "Login successful",
            icon: "success",
            text: "Welcome!"
        })   
        
    } else {

        //alert(`Please try again later`)
        Swal.fire({
            title: "Authentication failed",
            icon: "error",
            text: "Check your login details and try again"
        })
    }
})
// Clear input fields after submission
setEmail('');
setPassword('');

}

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);

return (    
        (user.id !== null) ?
            <Navigate to="/courses" />
        :
        <Form onSubmit={(e)=>authenticate(e)}>
            <h1 className="my-5 text-center">Login</h1>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>       
)
}
